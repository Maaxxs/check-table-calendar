import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';

class MyTableCalendar extends StatefulWidget {
  @override
  _MyTableCalendarState createState() => _MyTableCalendarState();
}

class _MyTableCalendarState extends State<MyTableCalendar> {
  CalendarController _calController;

  @override
  void initState() {
    super.initState();
    _calController = CalendarController();
  }

  @override
  void dispose() {
    _calController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TableCalendar(
        locale: 'de',
        startingDayOfWeek: StartingDayOfWeek.monday,
        calendarController: _calController,
      ),
    );
  }
}
